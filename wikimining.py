#!/usr/bin/env python

from urllib2 import build_opener
import HTMLParser

from HTMLParser import HTMLParser
URL = 'http://en.wikipedia.org'
ignore_urls = [
    '/wiki/Portal',
    '/wiki/Special',
    '/w/index.php',
    '/wiki/Help',
    'wiki/Wikipedia',
    '/wiki/Main_Page',
    '/wiki/Category',
    '/wiki/Template',
    '/wiki/Talk:',
    '/wiki/File:',
    
]

class LinkingHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        #super(LinkingHTMLParser, self).__init__()
        self.links = set()
    def handle_starttag(self, tag, attrs):
        if tag == "a" and len(attrs) > 1:
            (_,url), (_, title) = attrs[0], attrs[1]

            if url.startswith('/wiki') and not any(addy in url for addy in ignore_urls):
                #print 'Linked to %s at address: %s' % (title, url)
                self.links.add(url)


opener = build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]

class Page(object):
    '''
    A node on the internet... a webpage

        >>> Page('http://en.wikipedia.org/wiki/Nearest_neighbor_search')
    
    '''
    
    def __init__(self, url):
        self.url = url
        self.handle = opener.open(url)
        self.parser = LinkingHTMLParser()
    
    def __repr__(self):
        return 'Page(%s)' % self.url
    
    def __str__(self):
        return '\n'.join(line for line in self.handle)

    def parse(self):
        self.parser.feed(str(self))
        print 'Number of internal links: %d' % len(self.parser.links)
        
    def create_children(self):
        '''Create nodes for each link in self'''
        self.children = []
        
        for url in self.parser.links:
            child = Page(URL + url)
            self.children.append(
                child
                )
def find_n_most_related_pages(topic='Nearest neighbor search', n=10):
    topic = '/wiki/' + topic.replace(' ', '_')
    p = Page(URL + topic)
    p.parse()
    p.create_children()
    total_links = len(p.parser.links)
    all_links = p.parser.links
    link_counts = {}
    for child in p.children:
        try:
            child.parse()
            #print 'Child "%s" has %d links.' % (child.url, len(child.parser.links))
            total_links += len(child.parser.links)
            all_links = all_links.union(child.parser.links)
            for link in child.parser.links:
                if link in link_counts:
                    link_counts[link] += 1
                else:
                    link_counts[link] = 1
        except:
            print 'error'

    print 'Link counts:'
    for i, link in enumerate(sorted(link_counts, cmp=lambda x, y: link_counts[y] - link_counts[x])):
        if i <= n:
            print '%s has %d links.' % (link, link_counts[link])
    print '*' * 80
    print 'Total links only one node away: %d' % total_links
    print 'Unique pages only one node away: %d' % len(all_links)

find_n_most_related_pages('Cheese')

