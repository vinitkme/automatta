#! /bin/bash 

#Script to manage a directory with data of varios types.

echo "Enter the name of Directory to arrange"
echo 
read dir
mkdir $HOME/compressed



echo "Arranging music"
sudo find $dir -name '*.mp3' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Music' X '{}' +
sudo find $dir -name '*.aac' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Music' X '{}' +
sudo find $dir -name '*.flac' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Music' X '{}' +
sudo find $dir -name '*.m4a' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Music' X '{}' +
sudo find $dir -name '*.wma' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Music' X '{}' +
echo 

echo "Arranging Videos"
sudo find $dir -name '*.mkv' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Videos' X '{}' +
sudo find $dir -name '*.avi' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Videos' X '{}' +
sudo find $dir -name '*.wcp' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Videos' X '{}' +
sudo find $dir -name '*.mp4' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Videos' X '{}' +
sudo find $dir -name '*.mpeg4' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Videos' X '{}' +
sudo find $dir -name '*.flv' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Videos' X '{}' +
echo 

#Arranging Docments

echo "Arranging documents"
sudo find $dir -name '*.txt' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Documents' X '{}' +
sudo find $dir -name '*.pdf' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Documents' X '{}' +
sudo find $dir -name '*.doc*' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Documents' X '{}' +
sudo find $dir -name '*.ppt' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Documents' X '{}' +
sudo find $dir -name '*.html' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Documents' X '{}' +
sudo find $dir -name '*.xml' -type f -exec  sh -c 'exec cp -f "$@" $HOME/Documents' X '{}' +
echo 


echo "Arranging Compressed stuff:"
find $dir -name '*.zip' -type f -exec  sh -c 'exec cp -f "$@" $HOME/compressed' X '{}' +
find $dir -name '*.tar.gz' -type f -exec  sh -c 'exec cp -f "$@" $HOME/compressed' X '{}' +
find $dir -name '*.rar' -type f -exec  sh -c 'exec cp -f "$@" $HOME/compressed' X '{}' +
find $dir -name '*.iso' -type f -exec  sh -c 'exec cp -f "$@" $HOME/compressed' X '{}' +
find $dir -name '*.bz2' -type f -exec  sh -c 'exec cp -f "$@" $HOME/compressed' X '{}' +
find $dir -name '*.gz' -type f -exec  sh -c 'exec cp -f "$@" $HOME/compressed' X '{}' +
find $dir -name '*.z' -type f -exec  sh -c 'exec cp -f "$@" $HOME/compressed' X '{}' +
find $dir -name '*.tar.bz2' -type f -exec  sh -c 'exec cp -f "$@" $HOME/compressed' X '{}' +




