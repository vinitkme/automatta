#!/bin/bash
#===============================================================================
#
#          FILE:  setupserver.sh
# 
#         USAGE:  ./setupserver.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  |Vinit Kumar| (), |vinitcool76@gmail.com|
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  Tuesday 24 July 2012 10:54:53  IST IST
#      REVISION:  ---
#===============================================================================





ChoiceMaker () {

echo "This script will install Apache2 or nginx in your Linux box"
echo "==========================================================="
echo "1) apache2"
echo "2) nginx"
echo "Enter your option"
read option


case $option in 
	1)choice="apache2";;
        2)choice="nginx";;
        *)echo "Wrong Choice";;
esac

}

detectdistro () {

	if [[ -z $distro ]]; then
		distro="Unknown"
	fi 
		if which lsb_release > /dev/null 2>&1; then
			distro_detect=$(lsb_release -i | awk '{print $3}')
			if [[ "$distro_detect" == "archlinux" || "$distro_detect" == "Arch" ]];then 
				pacman -S $choice
			fi 

			if [[ "$distro_detect" == "Debian"|| "$distro_detect" == "LinuxMint" || "$distro_detect" == Ubuntu ]]; then sudo apt-get install $choice; fi 

			if [[ "$distro_detect" == "Fedora" ]]; then yum install $choice ; fi 

			if [ -f /etc/redhat-release ]; then yum install $choice; fi 
			if [ -f /etc/gentoo-release ]; then emerge $choice; fi 
		fi 
		}
		
	
if [ $UID == "0" ]; then 
ChoiceMaker
detectdistro
else 
	echo "Elevate your prevlidges to root user"
fi






