#! /usr/bin/env python


import simplejson,urllib,dict2xml
from BeautifulSoup import BeautifulStoneSoup



class Json2xml(object):

    def __init__(self,url):

        self.url = url

    def json2xml(self):

        result = simplejson.load(urllib.urlopen(self.url))
        content = dict2xml.dict2xml(result)
        soup = BeautifulStoneSoup(content)
        print soup.prettify()


def main():

    url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=ranchi'
    core = Json2xml(url)
    core.json2xml()

    print "Conversion done!"


if __name__ == "__main__":
    main()

