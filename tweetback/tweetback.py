#! /usr/bin/env python


import os.path
import twitter
import ConfigParser
from time import sleep
from random import random
from clint.textui import progress

config = ConfigParser.ConfigParser()

config.read(os.path.expanduser('~/.staty.conf'))
conskey = config.get("STATY","consumer_key",raw=True)
conssec = config.get("STATY","consumer_secret",raw=True)
accstkn = config.get("STATY","access_token",raw=True)
accssec = config.get("STATY","access_token_secret",raw=True)

api = twitter.Api(consumer_key=conskey,consumer_secret=conssec,access_token_key=accstkn,access_token_secret=accssec)


class TweetBack:
    'class to backup tweets'

    def __init__(self,user,count):
        self.user = user
        self.count = count

    def tweetback(self):
        'Change the file loction at your will.'
        feeds = api.GetUserTimeline(self.user,self.count)
        try:
            fp = open("/home/vicky/tweetback","w")
        except IOError,e:
            print "Error %s"%(e)
        fp.write("\n".join([feed.text for feed in feeds]))
        fp.close()


def main():

    print "Backing up your tweets!!"
    for i in progress.bar(range(100)):
        sleep(random()*0.2)
    backup = TweetBack("vinitcool76",20)
    backup.tweetback()

if __name__ == "__main__":
    main()











