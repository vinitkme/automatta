#!/bin/bash
#===============================================================================
#
#          FILE:  mailprep.sh
# 
#         USAGE:  ./mailprep.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  |Vinit Kumar| (), |vinitcool76@gmail.com|
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  Wednesday 01 August 2012 11:34:40  IST IST
#      REVISION:  ---
#===============================================================================


echo "Enter your email id"
read email

echo "Enter your password"
read -s passwd


echo "#! /bin/sh" >> mail.sh
echo "         ">>mail.sh

echo "curl https://$email:$passwd@mail.google.com/mail/feed/feed/atom -s">>mail.sh
echo "exit 0">>mail.sh
